package edu.tcs.app;


import java.text.DecimalFormat;
import java.util.ArrayList;

public class Aided extends Student{

	@Override
	public String result(String MarksOfStudent) {
		

		 String blankSpacesForStar= MarksOfStudent.replace(" ", "*");
		 String [] separetedMarks = blankSpacesForStar.split("\\|");
		
		 
		 
		 ArrayList<String> list = new ArrayList<String>();
		 String[] subjectMarks= separetedMarks[0].split(",");
		 for (int i = 0; i < subjectMarks.length; i++) {
				list.add(subjectMarks[i]);
			
			}
		 String[] NCCMarks= separetedMarks[1].split(",");
		 
		 String[] sportMarks = separetedMarks[2].split(",");
		 
		 double NCCMarksAvability = Double.parseDouble(NCCMarks[0]);
		 if(NCCMarksAvability!=0) {
					String nccToAdd =NCCMarks[1] +"*"+NCCMarks[2];
					list.add(nccToAdd);
		 }
		 double sportAvability = Double.parseDouble(sportMarks[0]);
		 if(sportAvability!=0) {
						
					String sportToAdd =sportMarks[1] +"*"+sportMarks[2];
					list.add(sportToAdd);
					

		 }	

		 
		double average = 0;
		double grade ;
		double credit;
		double totalCredits = list.size()*5;
		double CGPA = 0;

		String temp = list.toString();
		String temp2 = temp.replace("[", "");
		String temp3 = temp2.replace("]", "");
		String temp4= temp3.replace(" ","");
		String temp5 = temp4.replace("*", " ");
		
		 String [] separated=temp5.split(",");

		 for (int i = 0; i < separated.length; i++) {
		
			String [] separatedGrade = separated[i].split(" ");

			for (int j = 0; j < separatedGrade.length; j=j+2) {

				grade = Double.parseDouble(separatedGrade[j]);
				credit = Double.parseDouble(separatedGrade[j+1]);
				if(grade>74&&grade<101) {
		
						
						DecimalFormat df = new DecimalFormat("#.#");
						double points = 9+((grade-75)*.04);
							
						double roundpoints = Double.parseDouble(df.format(points));
						average += roundpoints* credit;
				
		
					
				}else if(grade>=60&&grade<75) {
					DecimalFormat df = new DecimalFormat("#.#");
					double points = 8+((grade-60)*.06);
					double roundpoints = Double.parseDouble(df.format(points));
					average +=roundpoints* credit;
				
				}else if(grade>=50&&grade<60) {
					if (grade==50) {
						double gradePoint=7;
						average += gradePoint* credit;
						
					}
					double points = 7+((grade-50)*.1);
					average += points* credit;
				
					
				}else if(grade>=40&&grade<50) {
					if (grade==40) {
						double gradePoint=6;
						average += gradePoint* credit;
					}
					double points = 6+((grade-40)*.1);
					average += points* credit;
				}else if(grade<40) {
					average+=0;
				}
		
				
				CGPA = average/totalCredits;
		
			}
			
		}
		
		String stringCGPA= String.format("%.2f", CGPA);
		return stringCGPA;
	}

	
}
