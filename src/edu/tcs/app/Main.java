package edu.tcs.app;


import java.text.DecimalFormat;


//Done it works fine
public class Main {
public static void main(String[] args) {
	
//	Aided aided = new Aided();
	SelfFinance self = new SelfFinance();
	
	//This is the string format that the problem provides me
	//I have to calculate the GPA base on a table
	/*For example:
	 * 67 4,34 2
	 * 
	 * 67 and 34 are the grade that the student reach
	 * 4 and 2 are the credits they will earn 
	 * The maximum number of credits is 5
	 * In order to calculate the GPA I need to perfom this operations
	 * 
	 * I will need to obtain how many points 67 and 34
	 * This is done by my if conditions in my Aid and Self aid clases
	 * 
	 * The equivalent in points of 67 is 8.42 points
	 * The equivalent in points of 34 is 0 because grades lesser than 40 earn 0 points
	 * 
	 * Then I will need to multiply the values  for the amount of credits
	 *         8.42 * 4   = 33.68  
	 * 		      0 * 3   = 0
	 * 
	 * After that I need to added them
	 * 
	 * 33.68 in total 
	 * 
	 * Each subject has a maximum amount of 5 credits
	 * In this case we have two subjects
	 * So the maximum is 5
	 * 
	 * The final GPA is  = 33.68 / 5 = 6.73
	 * 
	 * */
	
	String allMarks="67 4,34 2,54 5,100 2|1,100,5";

	//Marks in this format are special
	//|1,100,5| 
	//If they start with 1 they should be take in consideration for the GPA
	//If they start with 0 they should not be take in consideration for GPA
	
//	String resultado = aided.result(allMarks);
//	System.out.println(resultado);
//	int resultado2 = resultado.hashCode(); 	
//	System.out.println(resultado2);
	
	String resultado = self.result(allMarks);
	//This is my result
	System.out.println("Result obtained: "+resultado.hashCode());
	
	
	//This should be my result in hashcode form
	String myHash = "5.62";
	System.out.println("Expected value:  "+myHash.hashCode());
	//I am converting my result to hashcode because the problem test my program against the hashCode value
}
}
